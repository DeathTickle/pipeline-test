PREFIX=arm-none-eabi-
CC=$(PREFIX)gcc
OPT=--specs=nosys.specs -Os

all:
	$(CC) $(OPT) main.c
